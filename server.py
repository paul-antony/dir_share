import getpass
import socket
import os
import json

ip,port = "127.0.0.1",8000



def server_init():

	i = 3
	username = "User"
	password = "password"
	while i > 0:
		username = input("Username: ")
		password = getpass.getpass(prompt = 'Password')
		if password == getpass.getpass(prompt = 're-enter Password:'):
			break
		print("\nPassword missmatch\n")

		i = i-1
		if i == 0:
			print("\nall attempts failed\n")
			exit(0)	

	if username == "":
		username = "user"
	if password == "":
		password = "password"

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	s.bind((ip,port))
	print("ID: " + username + "@" + ip + ":" + str(port))

	return s, username, password

def prep(data,size = 1024):
	output = hex(data)[2:]
	for i in range(0,size - len(output)):
		output = "0" + output
		
	return output.encode()

def de_prep(data):
	output = int(data.decode(),16)
	return output


def server_auth(c, username, password):
	size = de_prep(c.recv(1024))
	user,pas = c.recv(size).decode().split("\n")
	if (user,pas) == (username, password):

		c.send(prep(len("Authentication Granted")))
		c.send("Authentication Granted".encode())

	else:
		c.send(prep(len("Authentication Failed")))
		c.send("Authentication Failed".encode())
		print("Unauthorised access server shutting down")
		exit(0)



def server_source_path(c):

	size = de_prep(c.recv(1024))
	source_path = c.recv(size).decode()
	if os.path.exists(source_path):

		c.send(prep(len("Path Exists")))
		c.send("Path Exists".encode())

	else:
		c.send(prep(len("Path not Exist")))
		c.send("Path not Exist".encode())
		print("Unauthorised path server shutting down")
		exit(0)



def directory_dict(path):
    d = {}
    if not os.path.isdir(path):
        return os.path.basename(path)
    else:
        d = {os.path.basename(path):[directory_dict(os.path.join(path,x)) for x in os.listdir(path)]}

    return d


def server_send_file(c):
	while(True):
		size = de_prep(c.recv(1024))
		path = c.recv(size).decode()

		if os.path.basename(path) == "index.index":

			pre = os.path.dirname(path)
			if os.path.isfile(pre):
				dir_struct = ["Is File"]

			else:
				dir_struct = [directory_dict(pre)]


			dir_struct = json.dumps(dir_struct)
			c.send(prep(len(dir_struct),1024))
			c.send(dir_struct.encode())

		elif path == "End Transition:":
			break

		else:
			f =  open(path,"rb")
			while True:
				data = f.read(1024)
				if not data:
					c.send(prep(len("END")))
					c.send("END".encode())
					break

				c.send(prep(len(data)))
				c.send(data)
			f.close()

def server():
	s, username, password = server_init()
	s.listen()
	c,addr = s.accept()
	server_auth(c, username, password)
	server_source_path(c)
	server_send_file(c)









if __name__ == "__main__":
	server()