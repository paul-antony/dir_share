import getpass
import socket
import os
import json




def prep(data,size = 1024):
	output = hex(data)[2:]
	for i in range(0,size - len(output)):
		output = "0" + output
		
	return output.encode()

def de_prep(data):
	output = int(data.decode(),16)
	return output

def client_init():
	username,addr = input("Enter server id:").split('@')   #id of format username@ip:port
	ip,port = addr.split(':')
	port = int(port)
	password = getpass.getpass()


	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((ip,port))
	data = username + "\n" + password

	s.send(prep(len(data)))
	s.send(data.encode())

	size = de_prep(s.recv(1024))
	access = s.recv(size).decode()
	print(access)

	if access == "Authentication Failed":
		exit(0)

	return s

def client_source_path(s):
	source_path = input("Enter source path:") #path of file in server
	s.send(prep(len(source_path)))
	s.send(source_path.encode())

	size = de_prep(s.recv(1024))
	access = s.recv(size).decode()
	if access == "Path not Exist":
		print(access)
		exit(0)


	dest_path = input("Enter destination path:")
	if dest_path == "." or dest_path == "./":
		dest_path = os.getcwd()
	return source_path, dest_path




def client_recive_index(s, source_path, dest_path):
	path = os.path.join(source_path,"index.index")
	s.send(prep(len(path)))
	s.send(path.encode())


	size = de_prep(s.recv(1024))
	data = s.recv(size).decode()
	dir_struct = json.loads(data)

	if dir_struct == ["Is File"]:
		client_recive_file(s, source_path, os.path.join(dest_path,os.path.basename(source_path)))

	else:

		client_recv_dir(s,dir_struct,os.path.dirname(source_path), dest_path)

	s.send(prep(len("End Transition:")))
	s.send("End Transition:".encode())	





def client_recive_file(s, source_path, dest_path):

	if not os.path.exists(os.path.dirname(dest_path)):
		os.makedirs(os.path.dirname(dest_path))

	s.send(prep(len(source_path)))
	s.send(source_path.encode())

	f = open(dest_path,"wb")
	while True:
		size = de_prep(s.recv(1024))
		data = s.recv(size)
		if data == b'END':
			break
		f.write(data)
	f.close()



def client_recv_dir(s,dir_struct,source_path, dest_path):
	for i in dir_struct:

		if type(i)== dict:

			key = list(i.keys())
			client_recv_dir(s, i[key[0]], os.path.join(source_path,key[0]) ,os.path.join(dest_path,key[0]))

		if type(i) == str:
			client_recive_file(s, os.path.join(source_path,i), os.path.join(dest_path,i))



def client():
	s = client_init()

	source_path, dest_path = client_source_path(s)
	client_recive_index(s, source_path, dest_path)

	#os.path.isfile(dest_path)


if __name__ == '__main__':
	client()